package com.example.eric.centralautobuses;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class RouteActivity extends FragmentActivity implements OnMapReadyCallback {

    private static final String LOGTAG = "centralautobuses";

    private GoogleMap mMap;

    private FirebaseDatabase database;
    private DatabaseReference myRef;

    private LatLng latLng;
    private Double latitud;
    private Double longitud;
    private List<LatLng> posicions = new ArrayList<>();
    private static final int INITIAL_STROKE_WIDTH_PX = 5;

    /**
     * El mètode onCreate es l'encarregat de fer la conexió amb Firebase, obtenir informació d'aquesta
     * mitjançant una Snapshot i afegir una nova PolyLine amb les dades obtenides, formant una ruta.
     *
     * El mètode onCancelled mostrarà un missatge d'error en cas de produïr-se un problema.
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_route);

        //Firebase
        database = FirebaseDatabase.getInstance();
        myRef = database.getReference();

        myRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                for (DataSnapshot singleSnapshot : dataSnapshot.child("root").child("xxx5678").getChildren()) {
                    latitud = singleSnapshot.child("latitud").getValue(Double.class);
                    longitud = singleSnapshot.child("longitud").getValue(Double.class);

                    latLng = new LatLng(latitud, longitud);
                    posicions.add(latLng);
                }

                mMap.addPolyline(new PolylineOptions()
                        .add(posicions.get(0), posicions.get(1), posicions.get(2), posicions.get(3))
                        .width(INITIAL_STROKE_WIDTH_PX)
                        .color(Color.BLUE)
                        .geodesic(true));

                float zoomLevel = 16.0f;
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(posicions.get(2), zoomLevel));

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(LOGTAG, "Error al intentar recuperar los datos.", databaseError.toException());
            }
        });

        //Google Maps
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    /**
     * El mètode onMapReady es l'encarregat de mostrar finalment el mapa amb totes les dades
     * recollides anteriorment (Ruta concreta)
     *
     * @param map
     */
    @Override
    public void onMapReady(GoogleMap map) {

        mMap = map;
        map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
    }
}
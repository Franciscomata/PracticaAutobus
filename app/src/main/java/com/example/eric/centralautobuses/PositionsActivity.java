package com.example.eric.centralautobuses;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

    public class PositionsActivity extends FragmentActivity implements OnMapReadyCallback {

        private static final String LOGTAG = "centralautobuses";

        private GoogleMap mMap;

    private FirebaseDatabase database;
    private DatabaseReference myRef;

    LatLng latLng;
    private Double latitud;
    private Double longitud;

    private final List<String> autobusos = new ArrayList<>();
    private final List<LatLng> posicions = new ArrayList<>();
    private final List<Float> markerColors = new ArrayList<>();

        /**
         * Al mètode onCreate s'inicialitza l'accès a la base de dades Firebase, s'afegeixen
         * els marcadors de colors per tal de diferenciar-los amb facilitat i s'obtè una Snapshot
         * de la base de dades per tal de recuperar dades i afegir els nous marcadors al mapa.
         *
         * El mètode onCancelled mostrarà un misstage d'error en cas de produïr-se algun problema.
         *
         * @param savedInstanceState
         */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_positions);

        //Firebase
        database = FirebaseDatabase.getInstance();
        myRef = database.getReference();

        //Colores de los marcadores
        markerColors.add(BitmapDescriptorFactory.HUE_RED);
        markerColors.add(BitmapDescriptorFactory.HUE_BLUE);
        markerColors.add(BitmapDescriptorFactory.HUE_MAGENTA);
        markerColors.add(BitmapDescriptorFactory.HUE_YELLOW);
        markerColors.add(BitmapDescriptorFactory.HUE_VIOLET);
        markerColors.add(BitmapDescriptorFactory.HUE_RED);
        markerColors.add(BitmapDescriptorFactory.HUE_BLUE);
        markerColors.add(BitmapDescriptorFactory.HUE_MAGENTA);
        markerColors.add(BitmapDescriptorFactory.HUE_YELLOW);
        markerColors.add(BitmapDescriptorFactory.HUE_VIOLET);

        //Query para obtener los últimos datos de B00001
        //Query latitudeQuery = myRef.child("B00001").orderByKey().limitToLast(1);
        //Query latitudeQuery = myRef.child("root").child("B00001").orderByKey().limitToLast(1);
        myRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                for (DataSnapshot singleSnapshot : dataSnapshot.child("root").getChildren()) {
                    autobusos.add(singleSnapshot.getKey());
                }

                for (int i = 0; i < autobusos.size(); i++) {

                    for (DataSnapshot singleSnapshot : dataSnapshot.child("root").child(autobusos.get(i)).getChildren()) {
                        latitud = singleSnapshot.child("latitud").getValue(Double.class);
                        longitud = singleSnapshot.child("longitud").getValue(Double.class);

                        latLng = new LatLng(latitud, longitud);
                    }

                    posicions.add(latLng);

                    mMap.addMarker(new MarkerOptions()
                            .position(posicions.get(i))
                            .title(autobusos.get(i))
                            .icon(BitmapDescriptorFactory.defaultMarker(markerColors.get(i))));
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(LOGTAG, "Error al intentar recuperar los datos.", databaseError.toException());
            }
        });

        //Google Maps
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

        /**
         * El mètode onMapReady es l'encarregat de mostrar finalment el mapa amb totes les dades
         * recollides anteriorment (Markers a posicions concretes)
         *
         * @param googleMap
         */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
    }
}
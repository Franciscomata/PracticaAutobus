package com.example.eric.centralautobuses;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MenuActivity extends Activity implements View.OnClickListener {

    /**
     * En el mètode onCreate s'inicia el layout activity_menu i
     * s'inicialitzen el botons encarregats d'anar als mapes
     * de posicions i de rutes.
     *
     * @param savedInstanceState
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        Button btnPosiciones = findViewById(R.id.buttonPosiciones);
        btnPosiciones.setOnClickListener(this);

        Button btnFacil = findViewById(R.id.buttonRutas);
        btnFacil.setOnClickListener(this);
    }

    /**
     * En el mètode onClick es fa l'acció d'anar a les activities corresponents a
     * mapa de posicions i mapa de rutes.
     *
     * @param view
     */
    @Override
    public void onClick(View view) {

        if (view.getId() == R.id.buttonPosiciones) {
            Intent i = new Intent(this, PositionsActivity.class);
            startActivity(i);
        }

        if (view.getId() == R.id.buttonRutas) {
            Intent i = new Intent(this, RouteActivity.class);
            startActivity(i);
        }
    }
}
